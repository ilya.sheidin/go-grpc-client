module gitlab.com/ilya.sheidin/git-grpc-client

go 1.14

require (
	gitlab.com/ilya.sheidin/git-grpc-blog-server v0.0.0-20200324140206-d6098d0553e5
	google.golang.org/grpc v1.28.0
)

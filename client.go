package main

import (
	"context"
	"fmt"
	blogpb "gitlab.com/ilya.sheidin/git-grpc-blog-server/proto"
	"google.golang.org/grpc"
	"io"
	"log"
	"os"
)

func main() {
	fmt.Println("Trying to connect")
	cc, err := grpc.Dial(fmt.Sprintf("%v:%v", os.Getenv("HOST"), os.Getenv("PORT")), grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to connect ot the server: %v", err)
	}
	//defer cc.Close()
	c := blogpb.NewBlogServiceClient(cc)
	nid, err := createBlog(c)
	if err != nil {
		log.Fatalf("Failed to cteate Blog: %v", err)
	}
	rBlog, err := readBlogById(nid, c)
	if err != nil {
		log.Fatalf("Failed to read blog: %v", err)
	}
	uBlog, err := updateBlogById(&blogpb.Blog{
		Id:       rBlog.GetId(),
		AuthorId: "Me",
		Title:    "Gusto",
		Content:  "Stu",
	}, c)
	if err != nil {
		log.Fatalf("Failed to update blog: %v", err)
	}
	err = deleteBlogById(uBlog.Id, c)
	if err != nil {
		log.Fatalf("Failed to delete blog: %v", err)
	}

	_, err = createBlog(c)
	_, err = createBlog(c)
	_, err = createBlog(c)
	if err != nil {
		log.Fatalf("Failed to cteate Blog: %v", err)
	}
	err = listBlogList(c)
	if err != nil {
		log.Fatalf("Failed to read list of blogs: %v", err)
	}
}

func createBlog(c blogpb.BlogServiceClient) (id string, err error) {
	res, err := c.CreateBlog(context.Background(), &blogpb.CreateBlogRequest{
		Blog: &blogpb.Blog{
			AuthorId: "1",
			Title:    "Hellou",
			Content:  "world",
		},
	})
	if err != nil {
		return "", err
	}

	fmt.Printf("Blog created with id %v \n", res.GetBlog().GetId())
	return res.GetBlog().GetId(), nil
}

func readBlogById(id string, c blogpb.BlogServiceClient) (*blogpb.Blog, error) {
	var b *blogpb.Blog
	res, err := c.ReadBlog(context.Background(), &blogpb.ReadBlogRequest{BlogId: id})
	if err != nil {
		return b, err
	}
	//b = res.GetBlog()
	fmt.Printf("Read done for id %v \n", res)
	return res.GetBlog(), nil
}

func updateBlogById(blog *blogpb.Blog, c blogpb.BlogServiceClient) (*blogpb.Blog, error) {
	var b *blogpb.Blog
	res, err := c.UpdateBlog(context.Background(), &blogpb.UpdateBlogRequest{
		Blog: blog,
	})
	if err != nil {
		return b, err
	}
	fmt.Printf("Update done for id %v \n", res.GetBlog().GetId())
	return res.GetBlog(), nil

}

func deleteBlogById(id string, c blogpb.BlogServiceClient) error {
	res, err := c.DeleteBlog(context.Background(), &blogpb.DeleteBlogRequest{
		BlogId: id,
	})
	if err != nil {
		return err
	}
	fmt.Println("Object removed:", res)
	return nil
}

func listBlogList(c blogpb.BlogServiceClient) error {
	stream, err := c.ListBlog(context.Background(), &blogpb.ListBlogRequest{})
	if err != nil {
		log.Fatalf("Failed to read stream: %v", err)
	}
	for {
		res, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Failed to read: %v", err)
		}
		fmt.Println(res.GetBlog())
	}
	return nil
}
